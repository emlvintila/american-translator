﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using AmericanTranslator.Handlers;
using AmericanTranslator.Handlers.Results;
using Discord;
using Discord.WebSocket;

namespace AmericanTranslator.Services
{
  public class MeasurementUnitTranslationService : IInitializeAsync
  {
    private readonly DiscordSocketClient client;

    public IChainHandler<IUserMessage, UserMessageChainHandler.MessageHandlerContext>? MessageHandler { get; set; }

    public MeasurementUnitTranslationService(DiscordSocketClient client) => this.client = client;

    public Task InitializeAsync()
    {
      client.MessageReceived += ClientOnMessageReceived;
      return Task.CompletedTask;
    }

    private async Task ClientOnMessageReceived(SocketMessage msg)
    {
      if (msg is not IUserMessage message)
        return;

      if (message.Author.IsBot || message.Author.IsWebhook)
        return;

      ImmutableList<IChainHandlerResult>? results = MessageHandler?
        .Handle(message, new UserMessageChainHandler.MessageHandlerContext(Enumerable.Empty<Range>()))
        .ToImmutableList();
      if (results is null || results.IsEmpty)
        return;

      IEnumerable<string> nonEmptyResults = results.Select(result => result.ToString()!).Where(line => !string.IsNullOrWhiteSpace(line));

      string reply = string.Join('\n', nonEmptyResults);
      if (string.IsNullOrWhiteSpace(reply))
        return;

      await message.Channel.SendMessageAsync(reply);
    }
  }
}
