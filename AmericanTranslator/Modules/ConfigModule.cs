﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AmericanTranslator.Data;
using AmericanTranslator.Model;
using AmericanTranslator.Model.DAO;
using AmericanTranslator.Utils;
using Discord;
using Discord.Commands;

namespace AmericanTranslator.Modules
{
  [RequireUserPermission(GuildPermission.ManageGuild)]
  public class ConfigModule : ModuleBase<SocketCommandContext>
  {
    private readonly ConfigRepository configRepository;
    private readonly ConfigManager configManager;

    public ConfigModule(ConfigRepository configRepository, ConfigManager configManager)
    {
      this.configRepository = configRepository;
      this.configManager = configManager;
    }

    [Command("config")]
    [Summary("Retrieves all config values")]
    private Task GetAllConfigs()
    {
      ulong guildId = Context.Guild.Id;
      ConfigDao dao = configRepository.GetForGuild(guildId);
      TranslatorConfig config = dao.Config;

      IEnumerable<string> descriptions = config.GetType().GetProperties().Select(propertyInfo => GetPropertyValueWithDescription(config, propertyInfo));

      string message = string.Join('\n', "All config properties:", string.Join('\n', descriptions));

      return ReplyAsync(message);
    }

    private string GetPropertyValueWithDescription(TranslatorConfig config, PropertyInfo propertyInfo)
    {
      StringBuilder sb = new StringBuilder();

      string name = propertyInfo.Name;
      object value = propertyInfo.GetValue(config)!;
      string valueString = configManager.GetConfigPropertyForDisplay(config, propertyInfo);
      sb.Append($"{name} = {valueString}");

      DescriptionAttribute? descriptionAttribute = propertyInfo.GetCustomAttribute<DescriptionAttribute>();
      string? description = descriptionAttribute?.Description;
      if (!string.IsNullOrWhiteSpace(description))
      {
        sb.AppendLine();
        sb.Append($"        {description}");
      }

      string allowedValues = ReflectionUtils.GetAllowedValues(value) ?? "any";
      sb.AppendLine();
      sb.Append($"        Allowed Values: {allowedValues}");

      return sb.ToString();
    }

    [Command("config")]
    [Summary("Retrieves a config value")]
    private Task GetConfigProperty(string propertyName)
    {
      ulong guildId = Context.Guild.Id;

      ConfigDao dao = configRepository.GetForGuild(guildId);
      TranslatorConfig config = dao.Config;
      try
      {
        PropertyInfo? propertyInfo = ReflectionUtils.GetPropertyInfo(config, propertyName);
        if (propertyInfo is null)
          return ReplyAsync("Invalid property.");

        string reply = GetPropertyValueWithDescription(config, propertyInfo);
        return ReplyAsync(reply);
      }
      catch (MissingMemberException)
      {
        return ReplyAsync("Invalid property.");
      }
    }

    [Command("config")]
    [Summary("Sets a config value")]
    private Task SetConfigProperty(string propertyName, string value)
    {
      ulong guildId = Context.Guild.Id;
      ConfigDao dao = configRepository.GetForGuild(guildId);
      TranslatorConfig config = dao.Config;

      Type type = config.GetType();
      PropertyInfo? propertyInfo = type.GetProperty(propertyName);
      if (propertyInfo is null)
        return ReplyAsync("Invalid property.");

      configManager.SetConfigProperty(config, propertyInfo, new[] {value});
      configRepository.UpdateForGuild(dao);

      return GetConfigProperty(propertyName);
    }
  }
}
