﻿using System.Collections.Generic;
using AmericanTranslator.Handlers.Results;

namespace AmericanTranslator.Handlers
{
  public abstract class ChainHandler<TMessage, TContext> : IChainHandler<TMessage, TContext>
  {
    public IChainHandler<TMessage, TContext>? Next { get; set; }

    public IEnumerable<IChainHandlerResult> Handle(TMessage message, TContext context)
    {
      (IChainHandlerResult result, TContext nextContext) = HandleInternal(message, context);
      List<IChainHandlerResult> results = new List<IChainHandlerResult> {result};
      // ReSharper disable once InvertIf
      if (Next != null)
      {
        IEnumerable<IChainHandlerResult> nextResult = Next.Handle(message, nextContext);
        results.AddRange(nextResult);
      }

      return results;
    }

    protected abstract (IChainHandlerResult result, TContext nextContext) HandleInternal(TMessage message, TContext context);
  }
}