﻿namespace AmericanTranslator.Handlers.Results
{
  public class TextHandlerResult : IChainHandlerResult
  {
    public TextHandlerResult(string text) => Text = text;

    public string Text { get; }

    public override string ToString() => Text;
  }
}
