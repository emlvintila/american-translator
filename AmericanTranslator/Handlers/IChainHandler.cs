﻿using System.Collections.Generic;
using AmericanTranslator.Handlers.Results;

namespace AmericanTranslator.Handlers
{
  public interface IChainHandler<TMessage, TContext>
  {
    public IChainHandler<TMessage, TContext>? Next { get; set; }

    public IEnumerable<IChainHandlerResult> Handle(TMessage message, TContext context);
  }
}