﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using Discord;

namespace AmericanTranslator.Handlers
{
  public abstract class UserMessageChainHandler : ChainHandler<IUserMessage, UserMessageChainHandler.MessageHandlerContext>
  {
    public class MessageHandlerContext
    {
      public MessageHandlerContext(IEnumerable<Range> excludedRanges) => ExcludedRanges = excludedRanges.ToImmutableArray();

      public IEnumerable<Range> ExcludedRanges { get; }
    }
  }
}