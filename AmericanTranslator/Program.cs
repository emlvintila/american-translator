﻿using System;
using System.Threading.Tasks;
using AmericanTranslator.Data;
using AmericanTranslator.Services;
using AmericanTranslator.Translators;
using AmericanTranslator.TypeBuilders;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using LiteDB;
using Microsoft.Extensions.DependencyInjection;
using StringBuilder = System.Text.StringBuilder;

namespace AmericanTranslator
{
  internal static class Program
  {
    private static async Task Main()
    {
      string token = Environment.GetEnvironmentVariable("AMERICAN_TRANSLATOR_TOKEN") ??
                     throw new Exception("No token specified. Specify a discord bot token through the AMERICAN_TRANSLATOR_TOKEN environment variable.");
      StringBuilder connectionString = new("Filename=main.db");
      #if DEBUG
      connectionString.Append("; Connection=shared");
      #endif
      using LiteDatabase db = new(connectionString.ToString());

      // TODO: Register all builders declared in the assembly
      TypeFactory typeFactory = new();
      typeFactory.RegisterBuilder(new IntBuilder());
      typeFactory.RegisterBuilder(new BoolBuilder());
      typeFactory.RegisterBuilder(new CharBuilder());
      typeFactory.RegisterBuilder(new TypeBuilders.StringBuilder());
      typeFactory.RegisterBuilder(new EnumBuilder());

      CommandService commandService = new(new CommandServiceConfig());

      DiscordSocketClient client = new();
      await client.LoginAsync(TokenType.Bot, token);
      await client.StartAsync();

      ServiceCollection services = new();
      // @formatter:off
      services
        .AddSingleton(db)

        .AddSingleton(typeFactory)

        .AddSingleton(client)

        .AddSingleton(commandService)
        .AddSingleton<CommandHandler>()

        .AddSingleton<ConfigManager>()
        .AddSingleton<ConfigRepository>()

        .AddSingleton<MeasurementUnitTranslationService>()

        // Allow to retrieve the list of registered services
        .AddSingleton(services);
      // @formatter:on

      IServiceProvider serviceProvider = services.BuildServiceProvider(new ServiceProviderOptions());

      // TODO: Register all property setters declared in the assembly
      ConfigManager _ = serviceProvider.GetService<ConfigManager>();

      // TODO: Initialize all IInitializeAsync instances
      await serviceProvider.GetService<CommandHandler>().InitializeAsync();

      MeasurementUnitTranslationService translationService = serviceProvider.GetService<MeasurementUnitTranslationService>();
      // @formatter:off
      translationService.MessageHandler =
               new InchesToCentimetersTranslator {
        Next = new CentimetersToInchesTranslator {
        Next = new FahrenheitToCelsiusTranslator {
        Next = new CelsiusToFahrenheitTranslator {
        Next = new PoundsToKilogramsTranslator {
        Next = new KilogramsToPoundsTranslator {
        Next = new FeetToMetersTranslator {
        Next = new MetersToFeetTranslator {
        Next = new YardsToMetersTranslator {
        Next = new MetersToYardsTranslator {
        Next = new MphToKmhTranslator {
        Next = new KmhToMphTranslator {
        Next = new MilesToKilometersTranslator {
        Next = new KilometersToMilesTranslator {
        Next = null
        }}}}}}}}}}}}}
      };
      // @formatter:on
      await translationService.InitializeAsync();

      await Task.Delay(-1);
    }
  }
}
