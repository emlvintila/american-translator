﻿using System.Threading.Tasks;

namespace AmericanTranslator
{
  public interface IInitializeAsync
  {
    Task InitializeAsync();
  }
}
