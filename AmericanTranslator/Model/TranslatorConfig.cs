﻿using System.ComponentModel;

 namespace AmericanTranslator.Model
{
  public class TranslatorConfig
  {
    [Description("The prefix that all commands should start with.")]
    public char CommandPrefix { get; set; } = '!';
  }
}
