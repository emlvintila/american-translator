﻿using LiteDB;

namespace AmericanTranslator.Model.DAO
{
  public class ConfigDao
  {
    [BsonId]
    public ulong GuildId { get; set; }

    public TranslatorConfig Config { get; set; } = new TranslatorConfig();
  }
}
