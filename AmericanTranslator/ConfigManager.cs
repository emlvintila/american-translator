﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AmericanTranslator.Model;
using AmericanTranslator.TypeBuilders;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace AmericanTranslator
{
  public class ConfigManager
  {
    private readonly IPropertyMapper<DefaultPropertyMapper.Context> defaultPropertyMapper = new DefaultPropertyMapper();
    private readonly IDictionary<string, object> registeredPropertyMappers = new Dictionary<string, object>();
    private readonly IServiceProvider serviceProvider;

    public ConfigManager(IServiceProvider serviceProvider) => this.serviceProvider = serviceProvider;

    public void RegisterPropertyMapper<TContext>(IPropertyMapper<TContext> propertyMapper)
    {
      registeredPropertyMappers.TryAdd(propertyMapper.PropertyName, propertyMapper);
    }

    public string GetConfigPropertyForDisplay(TranslatorConfig config, PropertyInfo propertyInfo)
    {
#pragma warning disable 8600
      object propertyMapper = registeredPropertyMappers.TryGetValue(propertyInfo.Name, out object registeredPropertyMapper)
#pragma warning restore 8600
        ? registeredPropertyMapper
        : defaultPropertyMapper;

      Type propertyMapperType = propertyMapper.GetType();
      Type propertyMapperInterface = propertyMapperType.GetInterface(typeof(IPropertyMapper<>).Name)!;
      InterfaceMapping interfaceMapping = propertyMapperType.GetInterfaceMap(propertyMapperInterface);
      MethodInfo method = interfaceMapping.TargetMethods.First(m => m.Name == nameof(IPropertyMapper<object>.GetPropertyForDisplay))!;

      return (string) method.Invoke(propertyMapper, new object[] {config, propertyInfo})!;
    }

    public void SetConfigProperty(TranslatorConfig config, PropertyInfo propertyInfo, IEnumerable args)
    {
#pragma warning disable 8600
      object propertyMapper = registeredPropertyMappers.TryGetValue(propertyInfo.Name, out object registeredPropertyMapper)
#pragma warning restore 8600
        ? registeredPropertyMapper
        : defaultPropertyMapper;

      Type propertyMapperType = propertyMapper.GetType();
      Type propertyMapperInterface = propertyMapperType.GetInterface(typeof(IPropertyMapper<>).Name)!;
      InterfaceMapping interfaceMapping = propertyMapperType.GetInterfaceMap(propertyMapperInterface);
      Type contextType = propertyMapperInterface.GetGenericArguments().First();

      // add the already registered services
      ServiceCollection extraServiceCollection = new ServiceCollection {serviceProvider.GetService<ServiceCollection>()};
      extraServiceCollection.AddSingleton(contextType);

      ServiceProvider extraServiceProvider = extraServiceCollection.BuildServiceProvider();

      // dependency inject the context
      object? contextInstance = extraServiceProvider.GetService(contextType);

      MethodInfo method = interfaceMapping.TargetMethods.First(m => m.Name == nameof(IPropertyMapper<object>.SetProperty))!;
      method.Invoke(propertyMapper, new[] {config, propertyInfo, args, contextInstance});
    }

    public interface IPropertyMapper<in TContext>
    {
      string PropertyName { get; }

      string GetPropertyForDisplay(TranslatorConfig config, PropertyInfo propertyInfo)
      {
        object? value = propertyInfo.GetValue(config);
        return Convert.ToString(value)!;
      }

      void SetProperty(TranslatorConfig config, PropertyInfo propertyInfo, IEnumerable args, TContext context);
    }

    public class DefaultPropertyMapper : IPropertyMapper<DefaultPropertyMapper.Context>
    {
      public string PropertyName { get; } = string.Empty;

      public void SetProperty(TranslatorConfig config, PropertyInfo propertyInfo, IEnumerable args, Context context)
      {
        Type propertyType = propertyInfo.PropertyType;
        object?[] buildArgs = args as object[] ?? args.Cast<object>().ToArray();
        object value = propertyType.IsEnum
          ? context.TypeFactory.GetBuilder<Enum>().Build(buildArgs.Prepend(propertyType))
          : context.TypeFactory.BuildType(propertyType, buildArgs);

        propertyInfo.SetValue(config, value);
      }

      public class Context
      {
        public Context(TypeFactory typeFactory) => TypeFactory = typeFactory;

        public TypeFactory TypeFactory { get; }
      }
    }
  }
}
