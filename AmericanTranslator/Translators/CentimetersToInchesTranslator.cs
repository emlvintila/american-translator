﻿using System.Text.RegularExpressions;
using AmericanTranslator.Utils;

namespace AmericanTranslator.Translators
{
  public class CentimetersToInchesTranslator : UnitTranslator
  {
    protected override Regex Regex { get; } = RegexUtils.MakeBoundaryRegex(RegexUtils.NUMBER_PATTERN + @"(?:cm|centimeters?|centimetres?)",
      RegexOptions.Compiled | RegexOptions.IgnoreCase);

    protected override string FromUnit { get; } = "cm";
    protected override string ToUnit { get; } = "in";

    protected override double TransformUnit(double from) => from / 2.54;
  }
}
