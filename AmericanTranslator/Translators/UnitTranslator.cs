﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text.RegularExpressions;
using AmericanTranslator.Handlers;
using AmericanTranslator.Handlers.Results;
using Discord;

namespace AmericanTranslator.Translators
{
  public abstract class UnitTranslator : UserMessageChainHandler
  {
    protected abstract Regex Regex { get; }
    protected abstract string FromUnit { get; }
    protected abstract string ToUnit { get; }

    protected abstract double TransformUnit(double from);

    protected override (IChainHandlerResult result, MessageHandlerContext nextContext) HandleInternal(IUserMessage message, MessageHandlerContext context)
    {
      IEnumerable<Match> matches = GetValidMatches(message, context);
      IEnumerable<(string ReplyLine, Range Range)> parseResult = ParseMatches(matches).ToImmutableArray();

      List<Range> nextExcludedRanges = new List<Range>(context.ExcludedRanges);
      nextExcludedRanges.AddRange(parseResult.Select(tuple => tuple.Range));

      IEnumerable<string> replyLines = parseResult.Select(tuple => tuple.ReplyLine);
      string joinedLines = string.Join('\n', replyLines);

      return (new TextHandlerResult(joinedLines), new MessageHandlerContext(nextExcludedRanges));
    }

    private IEnumerable<Match> GetValidMatches(IUserMessage message, MessageHandlerContext context)
    {
      return Regex
        .Matches(message.Content)
        .Where(match => !context.ExcludedRanges.Any(range => range.Start.Value <= match.Index && match.Index < range.End.Value));
    }

    private IEnumerable<(string ReplyLine, Range Range)> ParseMatches(IEnumerable<Match> matches)
    {
      // ReSharper disable once LoopCanBeConvertedToQuery
      foreach (Match match in matches)
      {
        double from = double.Parse(match.Groups[1].Value);
        double to = TransformUnit(from);

        string reply = $"{from:0.##}{FromUnit} = {to:0.##}{ToUnit}";
        Range range = match.Index .. (match.Index + match.Length);
        // Range range = ..0;

        yield return (reply, range);
      }
    }
  }
}
