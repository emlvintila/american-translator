﻿using System.Text.RegularExpressions;
using AmericanTranslator.Utils;

namespace AmericanTranslator.Translators
{
  public class PoundsToKilogramsTranslator : UnitTranslator
  {
    protected override Regex Regex { get; } = RegexUtils.MakeBoundaryRegex(RegexUtils.NUMBER_PATTERN + @"(?:lbs?|pounds?)",
      RegexOptions.Compiled | RegexOptions.IgnoreCase);

    protected override string FromUnit { get; } = "lbs";
    protected override string ToUnit { get; } = "kg";

    protected override double TransformUnit(double from) => from * 0.45359237;
  }
}
