﻿using System.Text.RegularExpressions;
using AmericanTranslator.Utils;

namespace AmericanTranslator.Translators
{
  public class FahrenheitToCelsiusTranslator : UnitTranslator
  {
    protected override Regex Regex { get; } = RegexUtils.MakeBoundaryRegex(RegexUtils.NUMBER_PATTERN + @"(?:deg|degrees?|°)?\s?F(?:ahrenheit)?",
      RegexOptions.Compiled | RegexOptions.IgnoreCase);

    protected override string FromUnit { get; } = "°F";
    protected override string ToUnit { get; } = "°C";

    protected override double TransformUnit(double from) => (from - 32) / 1.8;
  }
}
