﻿using System.Text.RegularExpressions;
using AmericanTranslator.Utils;

namespace AmericanTranslator.Translators
{
  public class FeetToMetersTranslator : UnitTranslator
  {
    protected override Regex Regex { get; } = RegexUtils.MakeBoundaryRegex(RegexUtils.NUMBER_PATTERN + @"(?:ft|feet|foot|′|')",
      RegexOptions.Compiled | RegexOptions.IgnoreCase);

    protected override string FromUnit { get; } = "ft";
    protected override string ToUnit { get; } = "m";

    protected override double TransformUnit(double from) => from * 0.3048;
  }
}
