﻿using System.Text.RegularExpressions;
using AmericanTranslator.Utils;

namespace AmericanTranslator.Translators
{
  public class YardsToMetersTranslator : UnitTranslator
  {
    protected override Regex Regex { get; } = RegexUtils.MakeBoundaryRegex(RegexUtils.NUMBER_PATTERN + @"(?:yds?|yards?)",
      RegexOptions.Compiled | RegexOptions.IgnoreCase);

    protected override string FromUnit { get; } = "yd";
    protected override string ToUnit { get; } = "m";

    protected override double TransformUnit(double from) => from * 0.9144;
  }
}
