﻿using System.Text.RegularExpressions;
using AmericanTranslator.Utils;

namespace AmericanTranslator.Translators
{
  public class KilogramsToPoundsTranslator : UnitTranslator
  {
    protected override Regex Regex { get; } = RegexUtils.MakeBoundaryRegex(RegexUtils.NUMBER_PATTERN + @"(?:kg|kilos?|kilograms?)",
      RegexOptions.Compiled | RegexOptions.IgnoreCase);

    protected override string FromUnit { get; } = "kg";
    protected override string ToUnit { get; } = "lbs";

    protected override double TransformUnit(double from) => from / 0.45359237;
  }
}
