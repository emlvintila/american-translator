﻿using System.Text.RegularExpressions;
using AmericanTranslator.Utils;

namespace AmericanTranslator.Translators
{
  public class CelsiusToFahrenheitTranslator : UnitTranslator
  {
    protected override Regex Regex { get; } = RegexUtils.MakeBoundaryRegex(RegexUtils.NUMBER_PATTERN + @"(?:deg|degrees?|°)?\s?C(?:elsius)?",
      RegexOptions.Compiled | RegexOptions.IgnoreCase);

    protected override string FromUnit { get; } = "°C";
    protected override string ToUnit { get; } = "°F";

    protected override double TransformUnit(double from) => from * 1.8 + 32;
  }
}
