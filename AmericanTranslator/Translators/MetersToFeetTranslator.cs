﻿using System.Text.RegularExpressions;
using AmericanTranslator.Utils;

namespace AmericanTranslator.Translators
{
  public class MetersToFeetTranslator : UnitTranslator
  {
    protected override Regex Regex { get; } = RegexUtils.MakeBoundaryRegex(RegexUtils.NUMBER_PATTERN + @"(?:m|meters?|metres?)",
      RegexOptions.Compiled | RegexOptions.IgnoreCase);

    protected override string FromUnit { get; } = "m";
    protected override string ToUnit { get; } = "ft";

    protected override double TransformUnit(double from) => from / 0.3048;
  }
}
