﻿using AmericanTranslator.Model.DAO;
using LiteDB;

namespace AmericanTranslator.Data
{
  public class ConfigRepository
  {
    private readonly LiteDatabase db;

    public ConfigRepository(LiteDatabase db) => this.db = db;

    public ConfigDao GetForGuild(ulong guildId)
    {
      db.Mapper.TrimWhitespace = false;
      db.Mapper.EmptyStringToNull = false;

      ConfigDao config = db.GetCollection<ConfigDao>().FindOne(c => c.GuildId == guildId) ??
                         UpdateForGuild(new ConfigDao {GuildId = guildId});

      return config;
    }

    public ConfigDao UpdateForGuild(ConfigDao config)
    {
      db.Mapper.TrimWhitespace = false;
      db.Mapper.EmptyStringToNull = false;

      db.GetCollection<ConfigDao>().Upsert(config);

      return config;
    }
  }
}
