﻿using System;
using System.Collections.Generic;

namespace AmericanTranslator.TypeBuilders
{
  public class EnumBuilder : TypeBuilderBase<Enum>
  {
    public override IEnumerable<Type> Parameters { get; } = new[] {typeof(Type), typeof(string)};

    protected override Enum BuildObject(object?[] args)
    {
      Type enumType = args[0] as Type ?? throw new ArgumentException(null, nameof(args));
      string enumValue = args[1] as string ?? throw new ArgumentException(null, nameof(args));

      return (Enum) Enum.Parse(enumType, enumValue);
    }
  }
}
