﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace AmericanTranslator.TypeBuilders
{
  public interface ITypeBuilder
  {
    IEnumerable<Type> Parameters { get; }

    object Build(IEnumerable args);
  }

  public interface ITypeBuilder<out T> : ITypeBuilder
  {
    new T Build(IEnumerable args);
  }
}
