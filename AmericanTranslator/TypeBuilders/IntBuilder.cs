﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AmericanTranslator.TypeBuilders
{
  public class IntBuilder : TypeBuilderBase<int>
  {
    public override IEnumerable<Type> Parameters { get; } = new[] {typeof(string)};

    protected override int BuildObject(object?[] args)
    {
      object? arg = args.First();

      return arg is null ? default : int.Parse((string) arg);
    }
  }
}
