﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace AmericanTranslator.TypeBuilders
{
  public class TypeFactory
  {
    private readonly IDictionary<Type, object> builderCache = new Dictionary<Type, object>();

    public void RegisterBuilder(Type type, ITypeBuilder builder)
    {
      if (builderCache.ContainsKey(type))
        return;

      builderCache.Add(type, builder);
    }

    public void RegisterBuilder<T>(ITypeBuilder<T> builder)
    {
      RegisterBuilder(typeof(T), builder);
    }

    public ITypeBuilder GetBuilder(Type type)
    {
      if (!builderCache.ContainsKey(type))
        throw new InvalidOperationException($"There is no builder registered for {type.FullName}");

      return (ITypeBuilder) builderCache[type];
    }

    public ITypeBuilder<T> GetBuilder<T>() => (ITypeBuilder<T>) GetBuilder(typeof(T));

    public object BuildType(Type type, IEnumerable args) => GetBuilder(type).Build(args);

    public T BuildType<T>(IEnumerable args) => (T) BuildType(typeof(T), args);
  }
}
