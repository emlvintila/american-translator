﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace AmericanTranslator.TypeBuilders
{
  public abstract class TypeBuilderBase<T> : ITypeBuilder<T>
  {
    public abstract IEnumerable<Type> Parameters { get; }

    object ITypeBuilder.Build(IEnumerable args) => Build(args)!;

    public T Build(IEnumerable args)
    {
      if (args == null)
        throw new ArgumentNullException(nameof(args));

      object?[] objects = args.Cast<object>().ToArray();

      if (objects.Length != Parameters.Count())
        throw new ArgumentException(null, nameof(args));

      return BuildObject(objects);
    }

    protected virtual T BuildObject(object?[] args)
    {
      // TODO: Cache the constructor
      ConstructorInfo? constructor = typeof(T).GetConstructor(Parameters.ToArray());
      if (constructor is null)
        throw new InvalidOperationException();

      T instance = (T) constructor.Invoke(args);
      return instance;
    }
  }
}
