﻿using System;
using System.Collections.Generic;

namespace AmericanTranslator.TypeBuilders
{
  public class StringBuilder : TypeBuilderBase<string>
  {
    public override IEnumerable<Type> Parameters { get; } = new[] {typeof(string)};

    protected override string BuildObject(object?[] args)
    {
      object? arg = args[0];
      if (arg is not null)
        return arg as string ?? arg.ToString()!;

      return string.Empty;
    }
  }
}
