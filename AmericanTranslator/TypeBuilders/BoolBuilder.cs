﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AmericanTranslator.TypeBuilders
{
  public class BoolBuilder : TypeBuilderBase<bool>
  {
    public override IEnumerable<Type> Parameters { get; } = new[] {typeof(string)};

    protected override bool BuildObject(object?[] args)
    {
      object? arg = args.First();

      return arg is not null && bool.Parse((string) arg);
    }
  }
}
