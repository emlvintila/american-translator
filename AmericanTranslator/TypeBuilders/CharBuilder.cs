﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AmericanTranslator.TypeBuilders
{
  public class CharBuilder : TypeBuilderBase<char>
  {
    public override IEnumerable<Type> Parameters { get; } = new[] {typeof(string)};

    protected override char BuildObject(object?[] args)
    {
      object? arg = args.First();

      // ReSharper disable once MergeConditionalExpression
      return arg is null ? default : ((string) arg).First();
    }
  }
}
