﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using AmericanTranslator.Data;
using AmericanTranslator.Model.DAO;
using Discord;
using Discord.Commands;
using Discord.WebSocket;

namespace AmericanTranslator
{
  public class CommandHandler
  {
    private readonly IServiceProvider services;
    private readonly CommandService commands;
    private readonly DiscordSocketClient client;
    private readonly ConfigRepository configRepository;

    public CommandHandler(IServiceProvider services, CommandService commands, DiscordSocketClient client, ConfigRepository configRepository)
    {
      this.services = services;
      this.commands = commands;
      this.client = client;
      this.configRepository = configRepository;
    }

    public async Task InitializeAsync()
    {
      await commands.AddModulesAsync(Assembly.GetEntryAssembly(), services);
      client.MessageReceived += HandleCommandAsync;
    }

    private async Task HandleCommandAsync(SocketMessage msg)
    {
      // Don't process the command if it was a system message
      if (msg is not SocketUserMessage message)
        return;

      if (message.Author.IsBot || message.Author is not IGuildUser guildUser)
        return;

      ulong guildId = guildUser.GuildId;
      ConfigDao config = configRepository.GetForGuild(guildId);
      char prefix = config.Config.CommandPrefix;

      // Create a number to track where the prefix ends and the command begins
      int argPos = 0;

      // Determine if the message is a command based on the prefix and make sure no bots trigger commands
      if (!message.HasCharPrefix(prefix, ref argPos) && !message.HasMentionPrefix(client.CurrentUser, ref argPos))
        return;

      SocketCommandContext context = new SocketCommandContext(client, message);

      await commands.ExecuteAsync(context, argPos, services);
    }
  }
}
