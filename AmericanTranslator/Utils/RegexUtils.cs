﻿using System.Text.RegularExpressions;

namespace AmericanTranslator.Utils
{
  public static class RegexUtils
  {
    public const string NUMBER_PATTERN = @"([+-]?(?:(?:\d+(?:[\.,]\d+)?)|(?:[\.,]\d+)))";

    public static Regex MakeBoundaryRegex(string pattern, RegexOptions options = RegexOptions.None) => new(@"(?<=^|\s)" + pattern + @"\b", options);
  }
}
