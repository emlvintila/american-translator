﻿using System;
using System.Reflection;

namespace AmericanTranslator.Utils
{
  public static class ReflectionUtils
  {
    public static PropertyInfo? GetPropertyInfo(object obj, string property)
    {
      Type type = obj.GetType();
      PropertyInfo? propertyInfo = type.GetProperty(property);

      return propertyInfo;
    }

    public static object? GetPropertyValue(object obj, string property)
    {
      PropertyInfo? propertyInfo = GetPropertyInfo(obj, property);
      if (propertyInfo is null)
        throw new MissingMemberException(obj.GetType().FullName, property);

      return propertyInfo.GetValue(obj);
    }

    public static void SetPropertyValue(object obj, string property, object? value)
    {
      Type type = obj.GetType();
      PropertyInfo? propertyInfo = type.GetProperty(property);
      if (propertyInfo is null)
        throw new MissingMemberException(type.FullName, property);

      propertyInfo.SetValue(obj, value);
    }

    public static string? GetAllowedValues(Type type)
    {
      // TODO: All all primitive types
      if (type == typeof(int))
        return $"[{int.MinValue}..{int.MaxValue}]";

      if (type == typeof(bool))
        return "{false, true}";

      if (type.IsEnum)
      {
        string[] names = Enum.GetNames(type);
        return $"{{{string.Join(", ", names)}}}";
      }

      return null;
    }

    public static string? GetAllowedValues(object obj) => GetAllowedValues(obj.GetType());
  }
}
